package dataBase;

import clases.Estudiante;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class File_Estudiante {
    
    public static void insertEstudiante(List<Estudiante> lstEstudiante) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");    
        int pos = 0;
        try {
            PrintWriter pw = new PrintWriter(new FileOutputStream("Estudiantes.txt"));
            for (Estudiante est : lstEstudiante) {
                pw.write(pos + "|"
                        + est.getColorBicicleta() + "|"
                        + est.getModeloBicicleta() + "|"
                        + est.getFoto() + "|"
                        + est.getEstadoEstudiante() + "|"
                        + est.getNombre() + "|"
                        + est.getApellido() + "|"
                        + est.getTipoDocumento() + "|"
                        + est.getNroDocumento() + "|"
                        + est.getSexo() + "|"
                        + dateFormat.format(est.getFechaEntrada()) + "|"
                        + dateFormat.format(est.getFechaSalida()) + "\n"
                );
                pos++;
            }
            pw.close();
        } catch (FileNotFoundException ex) {
            System.out.println("Excepción" + ex.getMessage());
        }
    }

    public static List<Estudiante> readEstudiante() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");    
        BufferedReader br = null;
        List<Estudiante> lstEstudiante = new LinkedList<>();

        try {
            br = new BufferedReader(new FileReader("Estudiantes.txt"));
            String linea = br.readLine();
            while (linea != null) {
                String[] vLinea = linea.split("\\|");
                Estudiante est  = new Estudiante(vLinea[1], vLinea[2], vLinea[3], vLinea[4],
                                                vLinea[5], vLinea[6], vLinea[7], vLinea[8], vLinea[9]);
                try {
                    est.setFechaEntrada(dateFormat.parse(vLinea[10]));
                    est.setFechaSalida(dateFormat.parse(vLinea[11]));
                } catch (ParseException ex) {
                    System.err.println(ex.getMessage());
                }
                
                lstEstudiante.add(est);
                linea = br.readLine();    
            }

        } catch (FileNotFoundException ex) {
            System.out.println("Excepcion Archivo no encontrado:" + ex.getMessage());

        } catch (IOException ex) {
            System.out.println("Excepcion de entrada o salida: " + ex.getMessage());

        } finally {
            try {
                br.close();
            } catch (IOException ex) {
                System.err.println(ex.getMessage());
            }
        }
        return lstEstudiante;
    }
}
