package dataBase;

import clases.Estudiante;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;

public class File_Estudiante_Historico {

    public static void insertHistoricStudent(List<Estudiante> lstEstudiante) {
        int pos = 0;
        try {
            PrintWriter pw = new PrintWriter(new FileOutputStream("HistoricoEstudiante.txt"));
            for (Estudiante est : lstEstudiante) {
                pw.write(pos + "|"
                        + est.getTipoDocumento() + "|"
                        + est.getNroDocumento() + "|"
                        + est.getNombre() + "|"
                        + est.getApellido() + "|"
                        + est.getEstadoEstudiante() + "|"
                        + est.getModeloBicicleta() + "|"
                        + est.getColorBicicleta() + "|"
                        + est.getSexo() + "|"
                        + est.getFoto() + "\n"
                );
            }
            pw.close();
        } catch (FileNotFoundException ex) {
            System.out.println("Excepción" + ex.getMessage());
        }
    }

    public static List<Estudiante> readStudentHistoric() {
        BufferedReader br = null;
        List<Estudiante> lstEstudiante = new LinkedList<>();

        try {
            br = new BufferedReader(new FileReader("HistoricoEstudiante.txt"));
            String linea = br.readLine();
            while (linea != null) {
                String[] vLinea = linea.split("\\|");
                Estudiante est  = new Estudiante(vLinea[5],
                                                    vLinea[6],
                                                    vLinea[8],
                                                    vLinea[4],
                                                    vLinea[2],
                                                    vLinea[3],
                                                    vLinea[0],
                                                    vLinea[1],
                                                    vLinea[8]);
                lstEstudiante.add(est);
                linea = br.readLine();
            }

        } catch (FileNotFoundException ex) {
            System.out.println("Excepcion Archivo no encontrado:" + ex.getMessage());

        } catch (IOException ex) {
            System.out.println("Excepcion de entrada o salida: " + ex.getMessage());

        } finally {
            try {
                br.close();
            } catch (IOException ex) {
                System.err.println(ex.getMessage());
            }
        }
        return lstEstudiante;
    }
}
