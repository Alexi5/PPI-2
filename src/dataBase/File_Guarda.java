
package dataBase;

import clases.Guarda;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;


public class File_Guarda {  
    public static void saveFileGuarda(List<Guarda> lstGrd){
        int pos = 0;
        try {
            PrintWriter pw = new PrintWriter( new FileOutputStream("Guarda.txt"));
            for (Guarda guarda : lstGrd) {
                pw.write(pos + "|" + 
                        guarda.getUsuario() + "|" +
                        guarda.getContraseña()+ "|" +
                        guarda.getPerfil()+ "|" +
                        guarda.isEstado()+ "|" +
                        guarda.getNombre() + "|" +
                        guarda.getApellido() + "|" +
                        guarda.getTipoDocumento() + "|" +
                        guarda.getNroDocumento() + "|" +
                        guarda.getSexo() + "\n"
                        );
            }
            pw.close();
        } catch (FileNotFoundException ex) {
             System.out.println("Excepción" + ex.getMessage());
        }
    }
    
    public static List<Guarda> read(){
        BufferedReader br = null;
        List<Guarda> lstGuarda = new LinkedList<>();
        try {
            br = new BufferedReader(new FileReader("Guarda.txt"));
            String linea = br.readLine();
            while (linea != null) {
                String[] vLinea = linea.split("\\|");
                Guarda grd = new Guarda(vLinea[1],vLinea[2],vLinea[3], true, vLinea[5], vLinea[6], vLinea[7], vLinea[8],vLinea[9]);
                lstGuarda.add(grd);
                linea = br.readLine();
            }
        }catch (FileNotFoundException ex) {
            System.out.println("Excepcion Archivo no encontrado:" + ex.getMessage());
        } catch (IOException ex) {
            System.out.println("Excepcion de entrada o salida: " + ex.getMessage());
        } finally {
            try {
                br.close();
            } catch (IOException ex) {
                System.err.println(ex.getMessage());
            }
        }
        
        return lstGuarda;
    }
    
}
