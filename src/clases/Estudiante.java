
package clases;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Estudiante extends Persona {
    private Date fechaEntrada;
    private Date fechaSalida;
    private String modeloBicicleta;
    private String colorBicicleta;
    private String foto;
    private String estadoEstudiante; 
     

    public Estudiante(String modeloBicicleta, String colorBicicleta, String foto, String tipoEstado, String nombre, String apellido, String tipoDocumento, String nroDocumento, String sexo) {
        super(nombre, apellido, tipoDocumento, nroDocumento, sexo);
        this.modeloBicicleta = modeloBicicleta;
        this.colorBicicleta = colorBicicleta;
        initDates();
        this.foto = foto;
        this.estadoEstudiante = tipoEstado; //Activo - Inactivo 
    }

        
    
    public Date getFechaEntrada() {
        return fechaEntrada;
    }

    public void setFechaEntrada(Date fechaEntrada) {
        this.fechaEntrada = fechaEntrada;
    }

    public Date getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(Date horaSalida) {
        this.fechaSalida = horaSalida;
    }

    public String getModeloBicicleta() {
        return modeloBicicleta;
    }

    public void setModeloBicicleta(String modeloBicicleta) {
        this.modeloBicicleta = modeloBicicleta;
    }

    public String getColorBicicleta() {
        return colorBicicleta;
    }

    public void setColorBicicleta(String colorBicicleta) {
        this.colorBicicleta = colorBicicleta;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getEstadoEstudiante() {
        return estadoEstudiante;
    }

    public void setEstadoEstudiante(String tipoEstado) {
        this.estadoEstudiante = tipoEstado;
    }
    
    public void initDates(){
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        try {
            this.fechaEntrada = dateFormat.parse("2017/01/01 00:00:00");
            this.fechaSalida = dateFormat.parse("2017/01/01 00:00:00");
        } catch (ParseException ex) {
            System.err.println(ex.getMessage());
        }
    }
}
