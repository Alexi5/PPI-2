/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author Alvaro
 */
public class Guarda extends Persona{
    private String horaEntrada;
    private String horaSalida;
    private String usuario;
    private String contraseña;
    private String perfil;
    private boolean estado;

    public Guarda(String usuario, String contraseña, String perfil, boolean estado, String nombre, String apellido, String tipoDocumento, String nroDocumento, String sexo) {
        super(nombre, apellido, tipoDocumento, nroDocumento, sexo);
        this.usuario = usuario;
        this.contraseña = contraseña;
        this.perfil = perfil;
        this.estado = estado;
    }

    

    
    


    public String getHoraEntrada() {
        return horaEntrada;
    }

    public void setHoraEntrada(String horaEntrada) {
        this.horaEntrada = horaEntrada;
    }

    public String getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(String horaSalida) {
        this.horaSalida = horaSalida;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }
    
    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }
   
    
    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    
    
    
    
}
