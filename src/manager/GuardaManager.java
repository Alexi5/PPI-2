
package manager;

import clases.Guarda;
import dao.GuardaDao;
import java.util.List;

public class GuardaManager {    
    private final GuardaDao guardaDao = new GuardaDao();
    private static String sessionVar;

    public GuardaManager() {
    }
    
    /**
     * Metodo que controla el ingreso de los usuario a el sistema
     * @param user
     * @param password
     * @return none
     */
    public boolean login(String user, String password){
        boolean result = false;
        List<Guarda> lstGuarda = guardaDao.getLstGuarda();
        
        for (Guarda guarda : lstGuarda) {
            if (guarda.getUsuario().equals(user) && guarda.getContraseña().equals(password)) {
                sessionVar = guarda.getNroDocumento();
                result = true;
                break;
            }                            
        }
        return result;
    }
    
    public boolean borrarUsuario(int pos){
       return guardaDao.eliminarGuarda( buscarInfoUsuarioPorPosicion(pos) );
    }
    
    public boolean  actualizarUsuario(Guarda grd){    
        return guardaDao.editarGuarda(grd);
    }
    
    public boolean insertarUsuario(Guarda usuario){
        return guardaDao.guardarGuarda(usuario);
    }
    
    public Guarda buscarInfoUsuario(){
        return guardaDao.buscarInfoGuardaRef(sessionVar);
    }
    
    public Guarda buscarInfoUsuarioPorPosicion(int posicion){
        return guardaDao.buscarGuarda(posicion);
    }
    
    public List<Guarda> getLstGuarda(){
        return guardaDao.getLstGuarda();
    }
}
