package manager;

import clases.Estudiante;
import java.util.Date;
import dao.EstudianteDao;
import dataBase.File_Estudiante;
import java.util.List;

/**
 *
 * @author johan.higuita
 */
public class EstudianteManager {
    private EstudianteDao estDao = new EstudianteDao();
    
    public boolean insertarEstudiante(Estudiante est){
        return estDao.guardarEstudiante(est);
    }
    
    public void registrarEntradaEstudiante(String docEstudiante){
        //DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Estudiante est = estDao.buscarEstudiantePorDocumento(docEstudiante);
        List<Estudiante> lstEstudiante = estDao.getLstEstudiante();
        
        for (Estudiante estudiante : lstEstudiante) {
            if (estudiante.getNroDocumento().equals(est.getNroDocumento())) {
                Date currentDate = new Date();
                estudiante.setFechaEntrada(currentDate);
            }
        }
        
    }
    
    public boolean validarEstudiante(String nroDocumento){
        List <Estudiante> lstEstudiante = estDao.getLstEstudiante();
        boolean res = false;
        for (Estudiante estudiante : lstEstudiante) {
            if (estudiante.getNroDocumento().equals(nroDocumento)) {
                res = true;
            }
        }
        
        return res;
    }
    
    
    public Estudiante returnEstudiante(String nroDocumento){
        List <Estudiante> lstEstudiante = estDao.getLstEstudiante();
        for (Estudiante estudiante : lstEstudiante) {
            if (estudiante.getNroDocumento().equals(nroDocumento)) {
                return estudiante;
            }
        }
        return null;
    }
    
    
 public int returnPosEstudiante(String nroDocumento){
        List <Estudiante> lstEstudiante = estDao.getLstEstudiante();
        int cont = 0;
        for (Estudiante estudiante : lstEstudiante) {
            if (estudiante.getNroDocumento().equals(nroDocumento)) {
                return cont;
            }
            cont ++;
        }
        return cont;
    }
 
    
    public boolean validarEntrada(String nroDocumento){
        boolean response = false;
        List <Estudiante> lstEstudiante = estDao.getLstEstudiante();
        try {
            int est = returnPosEstudiante(nroDocumento);
            Date currentTime = new Date();  
            lstEstudiante.get(est).setFechaEntrada(currentTime);
            File_Estudiante.insertEstudiante(lstEstudiante);
            response = true;
            
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        
        return response;        
    }
}
