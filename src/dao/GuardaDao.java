package dao;

import clases.Guarda;
import dataBase.File_Guarda;
import java.util.List;

public class GuardaDao {

    private List<Guarda> lstGuarda;

    public GuardaDao() {
        this.lstGuarda = File_Guarda.read();
//        llenarLista();
    }

    /**
     * Metodos Getter y setter para acceder a la lista de Los guardas
     *
     * @return
     */
    public List<Guarda> getLstGuarda() {
        return lstGuarda;
    }

    public void setLstGuarda(List<Guarda> lstGuarda) {
        this.lstGuarda = lstGuarda;
    }
    
    public Guarda buscarInfoGuardaRef(String documento) {
        Guarda result = null;

        for (Guarda guarda : lstGuarda) {
            if (guarda.getNroDocumento().equals(documento)) {
                result = guarda;
            }
        }
        return result;
    }

    public Guarda buscarGuarda(int pos) {
        return lstGuarda.get(pos);
    }


    public boolean guardarGuarda(Guarda grd) {
        boolean res = false;
        try {
            lstGuarda.add(grd);
            res = true;
        } catch (Exception e) {
            System.err.println("Error al insertar un guarda en la lista");
        }

        File_Guarda.saveFileGuarda(lstGuarda);
        this.lstGuarda = File_Guarda.read();
        return res;
    }

    public boolean eliminarGuarda(Guarda grd) {
        boolean response = false;
        for (Guarda guarda : lstGuarda) {
            if (guarda.getNroDocumento().equals(grd.getNroDocumento())) {
                try {
                    lstGuarda.remove(grd);
                    response = true;
                } catch (Exception e) {
                    System.err.println("Error al borrar un guarda");
                    System.err.println(e.getMessage());
                }
            }
        }
        File_Guarda.saveFileGuarda(lstGuarda);
        this.lstGuarda = File_Guarda.read();
        return response;
    }

    public boolean editarGuarda(Guarda grd) {
        boolean res = false;
        for (Guarda guarda : lstGuarda) {
            if (guarda.getNroDocumento().equals(grd.getNroDocumento())) {
                try {
                    guarda.setUsuario(grd.getUsuario());
                    guarda.setContraseña(grd.getContraseña());
                    guarda.setEstado(grd.isEstado());
                    guarda.setNombre(grd.getNombre());
                    guarda.setApellido(grd.getApellido());
                    guarda.setTipoDocumento(grd.getTipoDocumento());
                    guarda.setNroDocumento(grd.getNroDocumento());
                    guarda.setSexo(grd.getSexo());
                    // guarda.setFoto(grd.getFoto());
                    res = true;
                } catch (Exception e) {
                    System.err.println("Error al actualizar datos del guarda");
                    System.err.println(e.getMessage());
                }
            }
        }
        File_Guarda.saveFileGuarda(lstGuarda);
        this.lstGuarda = File_Guarda.read();
        return res;
    }
}
