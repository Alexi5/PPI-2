package dao;

import clases.Estudiante;
import dataBase.File_Estudiante;
import java.util.List;

public class EstudianteDao {
    private List<Estudiante> lstEstudiante; 

    public EstudianteDao() {
        lstEstudiante = File_Estudiante.readEstudiante();
    }
    
    /**
     * Metodos getters y setters para obtener la informacion de la lista
     * @return 
     */
    public List<Estudiante> getLstEstudiante() {
        return lstEstudiante;
    }

    public void setLstEstudiante(List<Estudiante> lstEstudiante) {
        this.lstEstudiante = lstEstudiante;
    }
    
    public boolean guardarEstudiante(Estudiante est){
        boolean res = false;
        try {
            lstEstudiante.add(est);
            File_Estudiante.insertEstudiante(lstEstudiante);
            res = true;
        } catch (Exception e) {
            System.err.println("Error al insertar un estudiante en la lista" +  e.getMessage());
        }
        return res;
        
    }
    
    public void eliminarEstudiante(Estudiante est){
        for (Estudiante estudiante : lstEstudiante) {
            if (estudiante.getNroDocumento().equals(est.getNroDocumento())) {
                try {
                    lstEstudiante.remove(est);
                } catch (Exception e) {
                    System.err.println("Error al borrar un estudiante");
                    System.err.println(e.getMessage());
                }
            }
        }
    }
    
    public void editarEstudiante(Estudiante est){
        for (Estudiante estudiante : lstEstudiante) {
            if (estudiante.getNroDocumento().equals(est.getNroDocumento())) {
                
                try {
                    estudiante.setModeloBicicleta(est.getModeloBicicleta());
                    estudiante.setColorBicicleta(est.getColorBicicleta());
                    estudiante.setFoto(est.getFoto());
                    estudiante.setEstadoEstudiante(est.getEstadoEstudiante());
                    estudiante.setNombre(est.getNombre());
                    estudiante.setApellido(est.getApellido());
                    estudiante.setTipoDocumento(est.getTipoDocumento());
                    estudiante.setNroDocumento(est.getNroDocumento());
                    estudiante.setSexo(est.getSexo());
                } catch (Exception e) {
                    System.err.println("Error al actualizar dato estudiante");
                    System.err.println(e.getMessage());
                }
            }
        }
    }
    
    public Estudiante buscarEstudiantePorDocumento(String documento) {
        for (Estudiante estudiante : lstEstudiante) {
            if (estudiante.getNroDocumento().equals(documento)) {
                return estudiante;
            }
        }
        return null;
    }
    
}
